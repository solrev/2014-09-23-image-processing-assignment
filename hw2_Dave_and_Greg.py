# -*- coding: utf-8 -*-
"""
Created on Fri Sep 26 20:53:28 2014

@author: solrev
"""

'''
define imports
import scientific libs
import image libs
import our own lib

create first frame
    image = our_lib_gen(desired width, height,vertices,initial rotation)

create an animation amtrix
    our_lib_animate(image, resolution, rate)

Generate an object to be plotted 
    imshow(image, use grey scale)
    
label the axes
    xlabel('')
    ylabel('')
    title('')

'''

#imports
import numpy as np
#import scipy as sp
from scipy import ndimage
import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
import hw2_Library as hl
#import cv2

#create a triangle on a page and return vertices info
#(width, height, # vertices, starting sigma)
temp, vertsz = hl.Generate_poly(256,256,3,0)

#apply a gaussian blur
gauss = ndimage.gaussian_filter(temp, sigma=3)

##find the edges with ndimage
#sx = ndimage.sobel(temp, axis=0, mode='constant')
#sy = ndimage.sobel(temp, axis=1, mode='constant')
#sob = np.hypot(sx, sy)
#
##harris corner 
#gray = cv2.cvtColor(temp,cv2.COLOR_BGR2GRAY)
#hs = cv2.cornerHarris(gray,2,3,0.04)
#
## Threshold for an optimal value, it may vary depending on the image.
#temp[dst>0.01*dst.max()]=[0,0,255]
#
#cv2.imshow('dst',temp)
#if cv2.waitKey(0) & 0xff == 27:
#    cv2.destroyAllWindows()
#    
#create a 3D array of image pages
#(image, vertices, pages, rate, time)
anime = hl.Rotate_me(temp,vertsz,30,10,2)

plt.figure(1)
plt.imshow(anime[:,:,0].transpose(), cmap=plt.cm.gray)
plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.title('My Triangle')

#show a couple frames
plt.figure(2)
plt.imshow(gauss.transpose(), cmap=plt.cm.gray)
plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.title('My Triangle')
#
#plt.figure(3)
#plt.imshow(anime[:,:,27].transpose(), cmap=plt.cm.gray)
#plt.xlabel('x-axis')
#plt.ylabel('y-axis')
#plt.title('My Triangle')