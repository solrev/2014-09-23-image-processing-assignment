# -*- coding: utf-8 -*-
"""
Created on Sat Sep 27 11:30:46 2014

@author: gregkline
"""

'''
Function Library

Here, we will keep the functions for homework 2
As they are going ot be of a large amount of code this will hepl keep the other
main file cleaner

generate(width, height, vertices #, sigma)
    image = create and array of size(width, height)
    generate a triangle on frame(image,vertices #, rotation)
    
generate triangle(image, vertices, rotation)
    vcreate a temporary hieght and width that will not cause 
    indeces to be exceeded
    
    v1: 
        place x on y axis (round to middle of width)
        place random positive height random integer (0, half height)
        
    v2/v3: 
        random x point random integer(up to axis or from axis)
        random negative y point
        
    for each x,y
        create a value 1
        
    use path command to find a path
        vertices = Path(x,y vertices)
        
    for each x point
        for each y point 
            if the point is contained in path
                make the point a value 1

rotate command(image, pages,rate)
    make an array of pages array(image dims, pages)
    make the first page of array the image of triangle array[:,:,0] = image
    
    turn list objects to arrayts
        array = np.array(list)
        
    for each page in pages       
        transform the vertices to the origin
            v = v-[width/2,height/2]
        rotate 
            v = [cos(theta) -sin(theta); sin(theta) cos(theta)]
        transform back
            v = v+[width/2,height/2]
        save new verts as verts
        perform same path algorithm
            make new path object
            find points
                for x in range
                    for y in range
                        is point in path?
                            yes = 1
        
        output new array
        
'''
import numpy as np
import random as rn
import math
from matplotlib.path import Path

#semi genertic generate function
def Generate_poly(width, height, vert_num, sigma):
    temp_image = np.zeros((height,width))
    vertices = np.array(vert_num)
    temp_image, vertices = Generate_vertices_and_Fill(temp_image, vert_num, sigma)

    return temp_image, vertices


#create the random vertices on the image
def Generate_vertices_and_Fill(image, vertices, sigma):
    #image information and then take into account 0 index
    width = image.shape[1] -1
    height = image.shape[0] -1
    
    #on positive y-axis
    v1x = math.floor(width/2)
    v1y = rn.randint(0,math.floor(height/2))
    
    #random quad III point
    v2x = rn.randint(0,math.floor(width/2))
    v2y = rn.randint(math.floor(height/2),height)
    
    #random quad IV point
    v3x = rn.randint(math.floor(width/2),width)
    v3y = rn.randint(math.floor(height/2),height)       
    
    #create vertices
    image[v1x,v1y] = 1
    image[v2x,v2y] = 1
    image[v3x,v3y] = 1
    
    #create a path vector for the connected triangle
    vertz = [(v1x,v1y),(v2x,v2y),(v3x,v3y),(v1x,v1y)]
    verts = Path(vertz)
    
    #check to see if points are in the path
    for i in range(0,width):
        for j in range(0,height):
            #if the point is contained it becomes a 1
            if verts.contains_point([i,j]):
                image[i,j] = 1
               
    return image, vertz

def Rotate_me(image,verts,pages,rate,time):
    #create animation pages
    animate_me = np.zeros((image.shape[0],image.shape[1],pages))
    animate_me[:,:,0] = image
    
    #find total movement over pages
    theta_total = float(rate * time)
    #pagewise rotation
    th = float(theta_total / pages)

    #create linear algebra transform matrix
    rt = [math.cos(th), math.sin(th)],[-math.sin(th), math.cos(th)]
    rt = np.array(rt)
    
    #turn lists into arrays to calculate
    verts = np.array(verts)
    new_v = np.array(verts)
    
    for p in range(1,pages):
        #linear transform to origin
        verts = new_v - [round(image.shape[1]/2),round(image.shape[0]/2)] 
        #replace values in new verts rotated
        for e in range(0,verts.shape[0]):
            new_v[e,:] = np.dot(rt,verts[e,:]) 
        #translate back again
        new_v = new_v + [round(image.shape[1]/2),round(image.shape[0]/2)]  
        #unpdate for next loop
        verts_p = Path(new_v)
        
        #check to see if points are in the path
        for i in range(0,image.shape[0]):
            for j in range(0,image.shape[1]):
                #if the point is contained it becomes a 1
                if verts_p.contains_point([i,j]):
                    animate_me[i,j,p] = 1
                               
    return animate_me
    